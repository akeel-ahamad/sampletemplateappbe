package com.ilink.sample.sampletemplateapp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ilink.sample.sampletemplateapp.model.User;
import com.ilink.sample.sampletemplateapp.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;

	public List<User> getAllUser()   
	{  
		List<User> user = new ArrayList<User>();  
		userRepository.findAll().forEach(user1 -> user.add(user1));  
		return user;  
	}  

	public User getUserById(int id)   
	{  
		return userRepository.findById(id).get();  
	}  

	public User saveOrUpdateUser(User useri)   
	{
		Optional<User> user = userRepository.findById(useri.getUserId());
		if(user.isPresent()) {
			User newEntity = user.get();
			newEntity.setEmail(useri.getEmail());
			newEntity.setName(useri.getName());
			newEntity.setContactNo(useri.getContactNo());
			newEntity = userRepository.save(newEntity);
			return newEntity;
		}else {
			userRepository.save(useri);  
			return useri;
		}

	}  

	public void deleteUser(int id)   
	{  
		userRepository.deleteById(id);  
	}  

}
