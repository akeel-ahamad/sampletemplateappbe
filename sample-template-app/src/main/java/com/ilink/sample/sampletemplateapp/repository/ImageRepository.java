package com.ilink.sample.sampletemplateapp.repository;


import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ilink.sample.sampletemplateapp.model.UploadImage;

@Repository
@Transactional
public interface ImageRepository extends JpaRepository<UploadImage, Long> {
	Optional<UploadImage> findByName(String name);
	List<UploadImage> deleteByName(String name);

}