package com.ilink.sample.sampletemplateapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleTemplateAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(SampleTemplateAppApplication.class, args);
	}

}
