package com.ilink.sample.sampletemplateapp.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;
import java.util.Optional;
import java.io.ByteArrayOutputStream;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.web.multipart.MultipartFile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ilink.sample.sampletemplateapp.model.UploadImage;
import com.ilink.sample.sampletemplateapp.model.User;
import com.ilink.sample.sampletemplateapp.repository.ImageRepository;
import com.ilink.sample.sampletemplateapp.service.ExportUsersService;
import com.ilink.sample.sampletemplateapp.service.UserService;


@RestController()
@RequestMapping("/user")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserController {

	@Autowired
	UserService userService; 

	@Autowired
	ImageRepository imageRepository;

	@Autowired
	ExportUsersService exportUsersService;

	@GetMapping("/")
	private List<User> getAllUser() {
		return userService.getAllUser();
	}

	@GetMapping("/{id}")  
	private User getUserById(@PathVariable int id)   
	{  
		return userService.getUserById(id);  
	} 

	@DeleteMapping("/{id}")  
	private void deleteUser(@PathVariable int id)   
	{  
		userService.deleteUser(id);
	}  

	@PostMapping("/")  
	private int saveUser(@RequestBody User user)   
	{  
		userService.saveOrUpdateUser(user);  
		return user.getUserId();
	}  

	@PutMapping("/")  
	private User update(@RequestBody User user)   
	{  
		userService.saveOrUpdateUser(user);  
		return user;
	}  


	@PostMapping("/uploadImage")
	public Map<String, String> uplaodImage(@RequestParam("profilePic") MultipartFile file) throws IOException {
		System.out.println("Original Image Byte Size - " + file.getBytes().length);
		UploadImage img = new UploadImage(file.getOriginalFilename(), file.getContentType(),file.getBytes());
		//				compressBytes(file.getBytes()));
		imageRepository.save(img);
		return new HashMap<String, String>(){{
			put("Status", "Ok");
		}};
	}

	@GetMapping(path = { "/image/{imageName}" })
	public UploadImage getImage(@PathVariable("imageName") String imageName) throws IOException {
		final Optional<UploadImage> retrievedImage = imageRepository.findByName(imageName);
		UploadImage img = new UploadImage(retrievedImage.get().getName(), retrievedImage.get().getType(),retrievedImage.get().getPicByte());
		//				decompressBytes(retrievedImage.get().getPicByte()));
		return img;
	}

	@DeleteMapping("/image/{imageName}")  
	private List<UploadImage> deleteImage(@PathVariable String imageName)   
	{  
		return imageRepository.deleteByName(imageName);
	}  

	// compress the image bytes before storing it in the database
	public static byte[] compressBytes(byte[] data) {
		Deflater deflater = new Deflater();
		deflater.setInput(data);
		deflater.finish();
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
		byte[] buffer = new byte[1024];
		while (!deflater.finished()) {
			int count = deflater.deflate(buffer);
			outputStream.write(buffer, 0, count);
		}
		try {
			outputStream.close();
		} catch (IOException e) {
		}
		System.out.println("Compressed Image Byte Size - " + outputStream.toByteArray().length);
		return outputStream.toByteArray();
	}

	// uncompress the image bytes before returning it to the angular application
	public static byte[] decompressBytes(byte[] data) {
		Inflater inflater = new Inflater();
		inflater.setInput(data);
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
		byte[] buffer = new byte[1024];
		try {
			while (!inflater.finished()) {
				int count = inflater.inflate(buffer);
				outputStream.write(buffer, 0, count);
			}
			outputStream.close();
		} catch (IOException ioe) {
		} catch (DataFormatException e) {
		}
		System.out.println("DeCompressed Image Byte Size - " + outputStream.toByteArray().length);
		return outputStream.toByteArray();
	}


	@GetMapping("/exportUsers")
	private Optional<Object> exportUsersAsPdf() {
		return exportUsersService.exportUsersAsPdf();
	}

}
